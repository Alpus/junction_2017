import cv2
import numpy as np
from flask import Flask, jsonify, make_response, request

from .pose_estimator import PoseEstimator


app = Flask(__name__)
pose_estimator = PoseEstimator()


def json_response(data, code=200):
    return make_response(jsonify(data), code)


def get_image_from_request(request):
    data = request.files['image']
    img = cv2.imdecode(np.fromstring(data.read(), np.uint8), cv2.IMWRITE_JPEG_QUALITY)
    return img # cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


@app.route('/', methods=['POST'])
def process():
    img = get_image_from_request(request)
    height = int(request.form.get('height'))
    width = int(request.form.get('width'))
    light_point = (height, width)

    return json_response(
        pose_estimator.check_hands(img, light_point)
    )
