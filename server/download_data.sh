#!/usr/bin/env bash

echo "Data downloading..."
wget https://www.dropbox.com/s/ae071mfm2qoyc8v/pose_model.pth?dl=1 -O model/pose_model.pth

echo "Done!"
