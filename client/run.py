#!/usr/bin/python3

from app import LightController


if __name__ == '__main__':
    camera_controller = LightController()
    camera_controller.run()
