import requests
from time import sleep, gmtime, strftime


import cv2

from . import light_controller


def img_to_formatted_str(img, img_type='jpg'):
    """
    Args:
        img (ndarray): input image in BGR
        img_type (str): extension of saved file ('jpg' or 'png')
    """

    is_converted, formatted_image = cv2.imencode('.{}'.format(img_type), img)

    if is_converted is False:
        raise ValueError("Can't convert image to {}".format(img_type))

    return formatted_image.tostring()


class LightController:
    _server_url = 'http://34.211.180.232:5000'

    def __init__(self, camera_url=1):
        self._video_capture = cv2.VideoCapture(camera_url)

    def _get_frame(self):
        _, frame = self._video_capture.read()
        return frame

    def _colibrate(self):
        """ NOT FINISHED, NOT IN USE

        Returns:

        """

        light_controller.turn_red_light_on()
        sleep(1)

        frame = self._get_frame()
        frame.argmax()

    def run(self):
        while True:
            frame = self._get_frame()
            frame = cv2.resize(frame, (256, 256))

            files = {'image': img_to_formatted_str(frame)}
            data = {'height': 55, 'width': 50}
            response = requests.post(self._server_url, files=files, data=data)

            if response.json() is True:
                if light_controller.is_light_off():
                    print(strftime('%H:%M:%S |', gmtime()), 'Turn RED')
                    light_controller.turn_red_light_on()
                    print('Done!')
                else:
                    print(strftime('%H:%M:%S |', gmtime()), 'Turn OFF')
                    light_controller.turn_light_off()
                    print('Done!')

                sleep(0.5)
