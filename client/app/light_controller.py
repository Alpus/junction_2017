from urllib.request import Request, urlopen
import requests


def turn_light_off():
    headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }

    request = Request('https://5nk8a0rfn5.execute-api.eu-west-1.amazonaws.com/v1/command?level=0&colour_x=7000000&colour_y=2500000', headers=headers)

    response_body = urlopen(request).read()
    return response_body


def turn_red_light_on():
    headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
    request = Request('https://5nk8a0rfn5.execute-api.eu-west-1.amazonaws.com/v1/command?level=254&colour_x=7000000&colour_y=2500000', headers=headers)

    response_body = urlopen(request).read()
    return response_body


def is_light_off():
    response = requests.get('https://5nk8a0rfn5.execute-api.eu-west-1.amazonaws.com/v1/dali-data?start=&limit=&end=')
    return response.json()[0]['nActual_Level'] == 0
